
//   the entry point into react. comonenet is rendered in public\index.html @root

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(

  <React.StrictMode>

    {/* this componant is defined in src\App.js  */}
  
        <App />
        </React.StrictMode>,



  document.getElementById('root')
  // <!-- target div for 'root' is public\index.html -->

);

 // eslint-disable-next-line no-lone-blocks
 {  // About serviceWorker
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
}

