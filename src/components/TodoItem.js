

import PropTypes from "prop-types";
import React, { Component } from 'react'




export class TodoItem extends Component {

	getStyle = () => {
		return {
			background: '#f4f4f4',
			padding: '10px',
			borderBottom: '1px #ccc dotted',
			textDecoration: this.props.todo.compleated ?
				'line-through' : 'none'
		}
	}







	render() {
		const { id, title } = this.props.todo

		return (
			<div style={this.getStyle()} >
				<p>
					<input type="checkbox" onChange={this.props.markComplete.bind(this, id)} /> {' '}
					{title}
					<button onClick={this.props.delTodo.bind(this, id)} style={btnStyle}>X</button>
				</p>
			</div>
		)
	}
}





// propTypes
TodoItem.propTypes = {
	todo: PropTypes.object.isRequired,
	markComplete: PropTypes.func.isRequired,
	delTodo: PropTypes.func.isRequired,
}

const btnStyle = {
	background: 'red',
	color: 'white',
	border: '1px',
	borderStyle: 'groove',
	borderColor: 'coral',
	padding: '5px 8px',
	// borderRadius: '30%',
	cursor: 'pointer',
	float: 'right'
}


export default TodoItem



