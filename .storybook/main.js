module.exports = {
  stories: ['../src/**/*.stories.js' ],
  // stories: ['../src/**/*.js'],
  addons: [
    '@storybook/preset-create-react-app',
    '@storybook/addon-actions',
    '@storybook/addon-links',
  ],
};
